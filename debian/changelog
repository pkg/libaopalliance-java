libaopalliance-java (20070526-7+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:40:23 +0530

libaopalliance-java (20070526-7+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 16:51:28 +0000

libaopalliance-java (20070526-7) unstable; urgency=medium

  * Team upload.
  * Removed the -java-doc package
  * Changed the priority from extra to optional
  * Standards-Version updated to 4.6.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 06 Dec 2022 00:08:02 +0100

libaopalliance-java (20070526-6+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:23 +0530

libaopalliance-java (20070526-6co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 00:37:27 +0000

libaopalliance-java (20070526-6) unstable; urgency=medium

  * Team upload.
  * Build with the DH sequencer instead of CDBS
  * Moved the package to Git
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 10
  * Fixed some typos in debian/copyright

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 12 Jan 2018 14:14:53 +0100

libaopalliance-java (20070526-5) unstable; urgency=low

  * Team upload.
  * Split documentation to a separate libaopalliance-java-doc package.
  * Reformat d/copyright to DEP-5.
  * Drop Depends on Java runtime since its a library.
  * Drop unneeded Depends on ${shlibs:Depends}.
  * Update Standards-Version: 3.9.2.
  * Update debhelper compat to 7.

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 07 Jan 2012 21:00:06 +0100

libaopalliance-java (20070526-4) unstable; urgency=low

  * Add POM file to package. (Closes: #581566)

 -- Torsten Werner <twerner@debian.org>  Sun, 16 May 2010 00:29:30 +0200

libaopalliance-java (20070526-3) unstable; urgency=low

  [ Thierry Carrez ]
  * debian/control: Use -headless runtimes
  * debian/ant.properties: Build java2 code to match dependency

  [ Torsten Werner ]
  * Switch to source format 3.0.
  * Fix Vcs-* headers.
  * Update Standards-Version: 3.8.4.

 -- Torsten Werner <twerner@debian.org>  Tue, 04 May 2010 21:50:27 +0200

libaopalliance-java (20070526-2) unstable; urgency=low

  * Change Maintainer: Debian Java Maintainers.
  * Update Standards-Version: 3.8.2:
    - Fix Homepage and Svn headers.
  * Change Section: java.
  * Switch to default-jdk.

 -- Torsten Werner <twerner@debian.org>  Sun, 28 Jun 2009 21:11:53 +0200

libaopalliance-java (20070526-1) unstable; urgency=low

  * Initial release (Closes: #426159)

 -- Torsten Werner <twerner@debian.org>  Sat, 26 May 2007 21:19:48 +0200

